# user namespace tests

To build the test run the following commands:
```bash
$ mkdir build
$ cd build
$ meson ..
$ ninja test
```
