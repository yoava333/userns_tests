#define _GNU_SOURCE
#include <err.h>
#include <errno.h>
#include <sched.h>
#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/mman.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>

#define ARRAY_SIZE(array) (sizeof(array) / sizeof(array[0]))
#define STACK_SIZE (1 << 20)

typedef int (*test_func_t)(void);
typedef int (*test_clone_func_t)(void *);

typedef struct test_cfg_s {
  const char *title;
  int clone_flags;
  int unshare_err;
  char *map_uid;
  char *setgroups;
  char *map_gid;
  uid_t cmp_uid;
  gid_t cmp_gid;
  struct test_cfg_s *next;
} test_cfg;

typedef struct {
  int fd;
  const test_cfg *test_case;
} clone_ctx;

// init uids
uid_t ruid = -1;
uid_t euid = -1;
uid_t ovf_uid = -1;

// init gids
gid_t egid = -1;
gid_t rgid = -1;
gid_t ovf_gid = -1;

void *stack = NULL;

int check(int r, const char *fmt, ...) {
  va_list va;

  va_start(va, fmt);
  if (r < 0) {
    verr(1, fmt, va);
  }
  va_end(va);

  return r;
}

static char *alloc_sprintf(const char *fmt, ...) {
  va_list va;
  char *buf = NULL;

  va_start(va, fmt);
  vasprintf(&buf, fmt, va);
  va_end(va);

  return buf;
}

char *read_file_proc(const char *path) {
  // proc files has no size, need to read one char at a time
  FILE *f = fopen(path, "r");

  int size = 100;
  int idx = 0;
  char *buf = malloc(size);
  if (!buf) {
    return NULL;
  }

  int ch = EOF;

  while (1) {
    ch = fgetc(f);

    if (ch == EOF) {
      break;
    }

    if (idx >= size + 1) {
      size = size * 2 + 1;
      buf = realloc(buf, size);
    }

    buf[idx] = (char)ch;
    idx++;
  }
  buf[idx] = '\0';

  return buf;
}

static int read_file_uint(const char *path, unsigned int *result) {

  int ret = 0;
  FILE *f = fopen(path, "r");

  if (!f) {
    return -1;
  }

  if (1 != fscanf(f, "%u", result)) {
    ret = -1;
  }

  fclose(f);

  return ret;
}

static int write_file(const char *path, const char *fmt, ...) {
  va_list va;
  int ret = 0;

  FILE *f = fopen(path, "w");

  if (!f) {
    return -1;
  }

  va_start(va, fmt);

  ret = vfprintf(f, fmt, va);

  va_end(va);

  fclose(f);

  if (ret < 0) {
    return ret;
  }

  return 0;
}

static int init() {
  // we need to setup the current user ids for tests

  int ret = -1;

  ruid = getuid();
  rgid = getgid();

  euid = geteuid();
  egid = getegid();

  ret = read_file_uint("/proc/sys/kernel/overflowuid", &ovf_uid);
  if (ret < 0) {
    return ret;
  }

  ret = read_file_uint("/proc/sys/kernel/overflowgid", &ovf_gid);
  if (ret < 0) {
    return ret;
  }

  stack = mmap(NULL, STACK_SIZE, PROT_WRITE | PROT_READ,
               MAP_PRIVATE | MAP_ANONYMOUS, -1, 0);
  if (stack == MAP_FAILED) {
    return -1;
  }

  return 0;
}

static void wait_for(int fd, char *s) {
  char buf[0x80];
  size_t len = strlen(s);

  if (len + 1 >= sizeof(buf)) {
    exit(1);
  }
  int ret = check(read(fd, buf, len + 1), "read");
  if (ret != len + 1) {
    exit(1);
  }
  if (strcmp(s, buf) != 0)
    exit(1);
}

int clone_newuser_wrap(const test_cfg *test_case);

int clone_clone_newuser_wrap_child(void *arg) {
  clone_ctx *ctx = (clone_ctx *)arg;
  const test_cfg *test_case = ctx->test_case;
  int ret = 0;

  printf("%s: %s: enter\n", __func__, test_case->title);

  wait_for(ctx->fd, "sync");

  if (getuid() != test_case->cmp_uid) {
    fprintf(stderr, "%s - getuid() = %u != cmp_uid = %u\n", test_case->title,
            getuid(), test_case->cmp_uid);
    ret = -1;
    goto end;
  }

  if (getgid() != test_case->cmp_gid) {
    fprintf(stderr, "%s - getgid() = %u != cmp_gid = %u\n", test_case->title,
            getgid(), test_case->cmp_gid);
    ret = -1;
    goto end;
  }

  if (test_case->next) {
    ret = clone_newuser_wrap(test_case->next);
  }

end:
  fprintf(stderr, "end: %s, ret = %d\n", test_case->title, ret);
  return ret;
}

int clone_newuser_wrap(const test_cfg *test_case) {
  int fds[2];
  int exit_code = 0;
  int status = 0;

  void *stack = mmap(NULL, STACK_SIZE, PROT_WRITE | PROT_READ,
                     MAP_PRIVATE | MAP_ANONYMOUS, -1, 0);
  if (stack == MAP_FAILED) {
    printf("failed to allocate stack, errno = %d\n", errno);
    return -1;
  }

  check(socketpair(AF_UNIX, SOCK_STREAM, 0, fds), "socketpair");

  clone_ctx ctx = {
      .fd = fds[0],
      .test_case = test_case,
  };

  int flags = test_case->clone_flags | SIGCHLD;

  int ret = clone(clone_clone_newuser_wrap_child, stack + STACK_SIZE, flags,
                  &ctx, NULL);
  int pid = ret;

  if (ret < 0) {
    // should fail?
    if ((ret < 0) != test_case->unshare_err) {
      fprintf(stderr, "start: %s - unshare failed, %s\n", test_case->title,
              strerror(errno));
      goto end;
    }

    // if unshare should fail, then return
    if (test_case->unshare_err) {
      ret = 0;
      goto end;
    }
  }
  ret = 0;

  // should we map pid
  if (test_case->map_uid) {
    ret =
        write_file(alloc_sprintf("/proc/%u/uid_map", pid), test_case->map_uid);
    if (ret < 0) {
      fprintf(stderr, "%s - write to uid_map failed, %s\n", test_case->title,
              strerror(errno));
      goto end;
    }
  }

  // should we set groups
  if (test_case->setgroups) {
    ret = write_file(alloc_sprintf("/proc/%u/setgroups", pid),
                     test_case->setgroups);
    if (ret < 0) {
      fprintf(stderr, "%s - write to setgroups failed, %s\n", test_case->title,
              strerror(errno));
      goto end;
    }
  }

  // should we map gid
  if (test_case->map_gid) {
    ret =
        write_file(alloc_sprintf("/proc/%u/gid_map", pid), test_case->map_gid);
    if (ret < 0) {
      fprintf(stderr, "%s - write to gid_map failed, %s\n", test_case->title,
              strerror(errno));
      goto end;
    }
  }

  check(write(fds[1], "sync", 5), "write");

  ret = -1;

  do {
    ret = waitpid(pid, &status, 0);
    if (ret < 0) {
      printf("waitpid failed, errno = %d\n", errno);
      goto end;
    }
  } while (!WIFEXITED(status));

  ret = 0;

  exit_code = WEXITSTATUS(status);
  if (exit_code != 0) {
    printf("exit_code: %d\n", exit_code);
    ret = -1;
  }

end:
  close(fds[0]);
  close(fds[1]);

  printf("%s: ret = %d\n", test_case->title, ret);
  return ret;
}

int test_unshare(test_cfg *test_case) {
  int ret = 0;
  fprintf(stderr, "start: %s\n", test_case->title);

  // should we unshare?
  if (test_case->clone_flags) {
    ret = unshare(test_case->clone_flags);

    // should fail?
    if ((ret < 0) != test_case->unshare_err) {
      fprintf(stderr, "start: %s - unshare failed, %s\n", test_case->title,
              strerror(errno));
      goto end;
    }

    // if unshare should fail, then return
    if (test_case->unshare_err) {
      ret = 0;
      goto end;
    }
  }

  // should we map pid
  if (test_case->map_uid) {
    ret = write_file("/proc/self/uid_map", test_case->map_uid);
    if (ret < 0) {
      fprintf(stderr, "%s - write to uid_map failed, %s\n", test_case->title,
              strerror(errno));
      goto end;
    }
  }

  // should we set groups
  if (test_case->setgroups) {
    ret = write_file("/proc/self/setgroups", test_case->setgroups);
    if (ret < 0) {
      fprintf(stderr, "%s - write to setgroups failed, %s\n", test_case->title,
              strerror(errno));
      goto end;
    }
  }

  // should we map gid
  if (test_case->map_gid) {
    ret = write_file("/proc/self/gid_map", test_case->map_gid);
    if (ret < 0) {
      fprintf(stderr, "%s - write to gid_map failed, %s\n", test_case->title,
              strerror(errno));
      goto end;
    }
  }

  if (getuid() != test_case->cmp_uid) {
    fprintf(stderr, "%s - getuid() = %u != cmp_uid = %u\n", test_case->title,
            getuid(), test_case->cmp_uid);
    ret = -1;
    goto end;
  }

  if (getgid() != test_case->cmp_gid) {
    fprintf(stderr, "%s - getgid() = %u != cmp_gid = %u\n", test_case->title,
            getgid(), test_case->cmp_gid);
    ret = -1;
    goto end;
  }

  if (test_case->next) {
    ret = test_unshare(test_case->next);
  }

end:
  fprintf(stderr, "end: %s, ret = %d\n", test_case->title, ret);
  return ret;
}

int run_unshare_test(test_cfg *test_case) {
  int ret = fork();

  if (ret < 0) {
    // error
    fprintf(stderr, "failed to fork, errno = %d\n", errno);
    return ret;
  } else if (ret == 0) {
    // child

    ret = test_unshare(test_case);
    exit(ret < 0);
  }
  // parent

  int status = 0;

  int child_pid = ret;
  do {
    ret = waitpid(child_pid, &status, 0);
    if (ret < 0) {
      fprintf(stderr, "failed to wait for child, errno = %d\n", errno);
      return ret;
    }

  } while (!WIFEXITED(status));

  int exit_code = WEXITSTATUS(status);

  if (exit_code != 0) {
    printf("exit code = %d\n", exit_code);
    return -1;
  }

  return 0;
}

int main() {

  check(init(), "init fail");

  test_cfg no_unshare = {
      .title = "test no unshare",
      .clone_flags = 0,
      .map_uid = NULL,
      .setgroups = NULL,
      .map_gid = NULL,
      .cmp_uid = euid,
      .cmp_gid = egid,
  };

  test_cfg unshare_no_map = {
      .title = "unshare_no_map",
      .clone_flags = CLONE_NEWUSER,
      .map_uid = NULL,
      .setgroups = NULL,
      .map_gid = NULL,
      .cmp_uid = ovf_uid,
      .cmp_gid = ovf_gid,
  };

  test_cfg unshare_map_uid = {
      .title = "unshare_map_uid",
      .clone_flags = CLONE_NEWUSER,
      .map_uid = alloc_sprintf("0 %u 1", euid),
      .setgroups = NULL,
      .map_gid = NULL,
      .cmp_uid = 0,
      .cmp_gid = ovf_gid,
  };

  test_cfg unshare_map_gid_no_setgroups = {
      .title = "unshare_map_gid_no_setgroups",
      .clone_flags = CLONE_NEWUSER,
      .map_uid = NULL,
      .setgroups = NULL,
      .map_gid = alloc_sprintf("0 %u 1", euid),
      .cmp_uid = ovf_uid,
      .cmp_gid = ovf_gid,
  };

  test_cfg unshare_map_gid_setgroups = {
      .title = "unshare_map_gid_setgroups",
      .clone_flags = CLONE_NEWUSER,
      .map_uid = NULL,
      .setgroups = "deny",
      .map_gid = alloc_sprintf("0 %u 1", euid),
      .cmp_uid = ovf_uid,
      .cmp_gid = 0,
  };

  test_cfg unshare_map_uid_gid_no_setgroups = {
      .title = "unshare_map_uid_gid_no_setgroups",
      .clone_flags = CLONE_NEWUSER,
      .map_uid = alloc_sprintf("0 %u 1", euid),
      .setgroups = NULL,
      .map_gid = alloc_sprintf("0 %u 1", euid),
      .cmp_uid = 0,
      .cmp_gid = ovf_gid,
  };

  test_cfg unshare_map_uid_gid_setgroups = {
      .title = "unshare_map_uid_gid_no_setgroups",
      .clone_flags = CLONE_NEWUSER,
      .map_uid = alloc_sprintf("0 %u 1", euid),
      .setgroups = "deny",
      .map_gid = alloc_sprintf("0 %u 1", euid),
      .cmp_uid = 0,
      .cmp_gid = 0,
  };

  // unshare should fail with EPERM because uid_map/gid_map  is not setup
  // correctly

  test_cfg unshare_fail = {
      .title = "unshare_fail",
      .clone_flags = CLONE_NEWUSER,
      .unshare_err = 1,
      .map_uid = NULL,
      .setgroups = NULL,
      .map_gid = NULL,
      .cmp_uid = ovf_uid,
      .cmp_gid = ovf_gid,
  };

  test_cfg unshare_recursive_no_map = {
      .title = "unshare_recursive_no_map",
      .clone_flags = CLONE_NEWUSER,
      .map_uid = NULL,
      .setgroups = NULL,
      .map_gid = NULL,
      .cmp_uid = ovf_uid,
      .cmp_gid = ovf_gid,
      .next = &unshare_fail,
  };

  // unshare should fail with EPERM because gid_map is not setup correctly

  test_cfg unshare_fail_map_uid = {
      .title = "unshare_fail_map_uid",
      .clone_flags = CLONE_NEWUSER,
      .unshare_err = 1,
      .map_uid = NULL,
      .setgroups = NULL,
      .map_gid = NULL,
      .cmp_uid = ovf_uid,
      .cmp_gid = ovf_gid,
  };

  test_cfg unshare_recursive_map_uid_no_gid_fail = {
      .title = "unshare_recursive_map_uid_no_gid_fail",
      .clone_flags = CLONE_NEWUSER,
      .map_uid = alloc_sprintf("0 %u 1", euid),
      .setgroups = "deny",
      .map_gid = NULL,
      .cmp_uid = 0,
      .cmp_gid = ovf_gid,
      .next = &unshare_fail_map_uid,
  };

  // map success

  test_cfg unshare_map_success = {
      .title = "unshare_map_success",
      .clone_flags = CLONE_NEWUSER,
      .map_uid = alloc_sprintf("0 %u 1", euid),
      .setgroups = "deny",
      .map_gid = alloc_sprintf("0 %u 1", euid),
      .cmp_uid = 0,
      .cmp_gid = 0,
  };

  test_cfg unshare_recursive_map_uid_gid = {
      .title = "unshare_recursive_map_uid_no_gid_fail",
      .clone_flags = CLONE_NEWUSER,
      .map_uid = alloc_sprintf("%u %u 1", euid, euid),
      .setgroups = "deny",
      .map_gid = alloc_sprintf("%u %u 1", euid, euid),
      .cmp_uid = euid,
      .cmp_gid = euid,
      .next = &unshare_map_success,
  };

  // map fail

  test_cfg unshare_map_fail = {
      .title = "unshare_map_uid_gid_no_setgroups",
      .clone_flags = CLONE_NEWUSER,
      .map_uid = alloc_sprintf("0 %u 1", euid + 1234),
      .setgroups = "deny",
      .map_gid = alloc_sprintf("0 %u 1", euid + 1234),
      .cmp_uid = ovf_uid,
      .cmp_gid = ovf_gid,
  };

  test_cfg *tests[] = {
      // single unshare
      &no_unshare,
      &unshare_no_map,
      &unshare_map_uid,
      &unshare_map_gid_no_setgroups,
      &unshare_map_gid_setgroups,
      &unshare_map_uid_gid_no_setgroups,
      &unshare_map_uid_gid_setgroups,
      &unshare_map_fail,

      // recursive
      &unshare_recursive_no_map,
      &unshare_recursive_map_uid_no_gid_fail,
      &unshare_recursive_map_uid_gid,
  };

  // tests fork + unshare
  for (int i = 0; i < ARRAY_SIZE(tests); i++) {
    test_cfg *test = tests[i];
    printf("[%d] %s\n", i, test->title);
    check(run_unshare_test(test), test->title);
  }

  // Same tests with clone + CLONE_NEWUSER
  for (int i = 0; i < ARRAY_SIZE(tests); i++) {
    test_cfg *test = tests[i];

    printf("[%d] %s\n", i, test->title);
    check(clone_newuser_wrap(test), test->title);
  }

  return 0;
}
